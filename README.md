# ATOMDB 共建

#### 软件运行图
<img src = "https://gitee.com/pulsarware/atomdb-proposals/raw/master/resources/images/atomdb_screenshot.png" />

#### 用途介绍
为了更好的开发出满足广大开发者朋友的工具软件，我们创建了 ATOMDB 共建库：**好软件，一起建！**

共建库当前结构如下：
```
|- 头脑风暴
|- 正式提案
|-- v0.0.1.md
|-- v0.0.2.md
|-- ...
```

##### 头脑风暴
大家开始的提供最终先归集到这个文件夹下面，一个提案一个 markdown 文件，文件名称根据功能进行命名。

##### 正式提案
进过充分讨论之后的提案会被最终移到这里，并且规划到特定的版本进行实现，我们会以版本号为名称创建对应的功能提案集合。

#### 产品介绍

ATOMDB™ 数据库管理客户端是一款服务广大开发者的工具软件。可以让开发者方便的执行查询，创建和管理数据库，支持管理市场上主流的数据库管理系统。例如： TIDB, PolarDB, GreatDB, 人大金仓, 神舟通用, TDengine, MySQL, PostgreSQL, Microsoft SQL Server, Oracle 等等。软件设计支持 Windows、MacOS、Linux 等操作系统，软件采用插件扩展的方式对多数据库进行支持。
软件支持如下主要功能：

1. 多数据库系统支持
2. 项目化管理数据库链接会话实例
3. SQL 语法高亮和自动代码补全
4. 数据导入导出
5. 现代的用户设计界面
6. 版本控制软件支持

#### 秦派软件介绍

<img src = "https://gitee.com/pulsarware/atomdb-proposals/raw/master/resources/images/pulsarware.png"/>

极语言信息技术有限公司(Pulsarware™ Technologies Ltd.)是一家致力于开发国产生产力工具软件的技术公司，中国的信息产业非常发达，特别是互联网应用领域，但是在基础软件领域我们却很少有知名的软件产品，在这一领域大多是国外的产品。极语言信息技术有限公司(Pulsarware™ Technologies Ltd.)自成立以来就立志要在工具软件领域进行深耕，努力推出几款优质的工具软件服务国内的开发者。星星之火可以燎原，相信经过大家的努力，在基础软件领域一定会有越来越多的国内的公司推出自己的产品。

如果您对秦派软件感兴趣，欢迎访问：https://atomdb.com